#ifndef SUPERMERCADO_H
#define SUPERMERCADO_H

#include "Fornecedor.h"
#include "Cliente.h"
#include "Estabelecimento.h"
#include "Produto.h"
#include "vector_supermercado.h"

#include <iostream>
#include <string>
#include <vector>

class Supermercado : public Estabelecimento {
	public:
		Supermercado();
		~Supermercado();

		void listar();
		void vender(std::string produto, int quantidade, Cliente &cliente);
		void reabastecer(std::string p, int q, Fornecedor &f);
		void caixa();
		void imprimir_menu(Cliente &cliente);
		void listar_produtos_estoque();
		void listar_caixa();
		void iniciar_compra(Cliente &cliente);
		void ver_sacola(Cliente &cliente);
		void listar_produtos_fornecedor(Fornecedor &fornecedor);
		void iniciar_reabastecimento(Fornecedor &fornecedor);
		void sair();
		void entrar(Fornecedor &fornecedor, Cliente &cliente);
};

#endif