#ifndef CLIENTE_H
#define CLIENTE_H

#include "Produto.h"
#include "vector_supermercado.h"

#include <iostream>
#include <string>
#include <vector>

class Cliente {
	public:
		int id;
		double saldo;
		vector_supermercado<Produto> sacola;
		vector_supermercado<Produto> comido;

		Cliente(int id);

		void comprar(Produto produto, int quantidade);
		void comer(Produto produto, int quantidade);
		void verSacola();
		void verConta();
		void registro_supermercado();
		void registro_restaurante();
};

#endif