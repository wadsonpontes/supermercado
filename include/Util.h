#ifndef UTIL_H
#define UTIL_H

#ifdef __WIN32__
#define LIMPAR_TELA "cls"
#else
#define LIMPAR_TELA "clear"
#endif

#include <string>
#include <fstream>

std::ifstream open_file_for_reading(std::string filename);
std::ofstream open_file_for_writing(std::string filename);
void check_file_for_error(bool error);
int limparTela();
void pausar();
void comando_inexistente();

#endif