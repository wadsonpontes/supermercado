#ifndef FORNECEDOR_H
#define FORNECEDOR_H

#include "Produto.h"
#include "vector_supermercado.h"

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

class Fornecedor {
	public:
		std::ifstream arquivo_leitura;
		std::ofstream arquivo_escrita;
		vector_supermercado<Produto> produtos;

		Fornecedor();
		~Fornecedor();

		void listarProdutos();
		int repassarProdutos(std::string produto, int quantidade);
		void salvar();
};

#endif