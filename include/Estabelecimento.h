#ifndef ESTABELECIMENTO_H
#define ESTABELECIMENTO_H

#include "Produto.h"
#include "Util.h"
#include "Fornecedor.h"
#include "vector_supermercado.h"
#include "Cliente.h"

#include <iostream>
#include <string>
#include <list>
#include <fstream>

class Estabelecimento {
	public:
		std::ifstream arquivo;
		vector_supermercado<Produto> produtos;
		vector_supermercado<Produto> vendidos;
};

#endif