#ifndef RESTAURANTE_H
#define RESTAURANTE_H

#include "Cliente.h"
#include "Estabelecimento.h"
#include "Produto.h"
#include "vector_supermercado.h"

#include <iostream>
#include <string>
#include <vector>

class Restaurante : public Estabelecimento {
	public:
		Restaurante();
		~Restaurante();

		void listar();
		void vender(std::string produto, int quantidade, Cliente &cliente);
		void caixa();
		void imprimir_menu(Cliente &cliente);
		void listar_produtos_menu();
		void listar_caixa();
		void iniciar_pedido(Cliente &cliente);
		void ver_conta(Cliente &cliente);
		void sair();
		void entrar(Cliente &cliente);
};

#endif