#ifndef PRODUTO_H
#define PRODUTO_H

#include <iostream>
#include <string>
#include <vector>

class Produto {
	public:
		int codigo;
		std::string produto;
		std::string unidadeMedida;
		double preco;
		int quantidade;

		Produto();
};

#endif