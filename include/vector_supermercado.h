#ifndef VECTOR_H
#define VECTOR_H

#include "Produto.h"

#include <iostream>
#include <string>
#include <vector>

template <class T> class vector_supermercado {
	public:
		size_t tamanho;
		size_t capacidade;
		T *vetor;

		vector_supermercado() {
			this->tamanho = 0;
			this->capacidade = 10;
			this->vetor = new T[this->capacidade];
		}

		~vector_supermercado() {
			// delete[] this->vetor;
		}

		T& operator [](int idx) {
			return vetor[idx];
		}

		T operator [](int idx) const {
			return vetor[idx];
		}

		T& operator [](std::string idx) {
			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == idx)
					return this->vetor[i];
			}

			return this->vetor[0];
		}

		T operator [](std::string idx) const {
			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == idx)
					return this->vetor[i];
			}

			return this->vetor[0];
		}

		T& operator [](Produto idx) {
			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == idx.produto)
					return this->vetor[i];
			}

			return this->vetor[0];
		}

		T operator [](Produto idx) const {
			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == idx.produto)
					return this->vetor[i];
			}

			return this->vetor[0];
		}

		void push(T e) {
			if(this->tamanho == this->capacidade) {
				this->redimensionar();
			}
	
			this->vetor[this->tamanho] = e;

			this->tamanho++;
		}

		bool existe(Produto produto) {
			bool e = false;

			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == produto.produto)
					return true;
			}

			return e;
		}

		bool existe(std::string produto) {
			bool e = false;

			for (size_t i = 0; i < this->tamanho; ++i) {
				if (this->vetor[i].produto == produto)
					return true;
			}

			return e;
		}

		void redimensionar() {
			T *v = new T[this->capacidade * 2];

			for (size_t i = 0; i < this->tamanho; ++i) {
				v[i] = this->vetor[i];
			}

			delete[] this->vetor;

			this->vetor = v;
			this->capacidade *= 2;
		}
};

#endif