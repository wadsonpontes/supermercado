CC = g++
CFLAGS = -std=c++11 -Iinclude -Wall -O2
PROG = Supermercado

$(PROG): clean main.o Supermercado.o Restaurante.o Estabelecimento.o Cliente.o Produto.o Fornecedor.o vector_supermercado.o Excecao.o Util.o
	$(CC) $(CFLAGS) -o build/Supermercado *.o

clean:
	rm -f src/*.o
	rm -f *.o
	rm -f build/Supermercado
	rm -f build/Supermercado.exe

main.o: src/main.cpp
	$(CC) $(CFLAGS) -c src/main.cpp

Supermercado.o: src/Supermercado.cpp include/Supermercado.h
	$(CC) $(CFLAGS) -c src/Supermercado.cpp

Restaurante.o: src/Restaurante.cpp include/Restaurante.h
	$(CC) $(CFLAGS) -c src/Restaurante.cpp

Estabelecimento.o: src/Estabelecimento.cpp include/Estabelecimento.h
	$(CC) $(CFLAGS) -c src/Estabelecimento.cpp

Cliente.o: src/Cliente.cpp include/Cliente.h
	$(CC) $(CFLAGS) -c src/Cliente.cpp

Produto.o: src/Produto.cpp include/Produto.h
	$(CC) $(CFLAGS) -c src/Produto.cpp

Fornecedor.o: src/Fornecedor.cpp include/Fornecedor.h
	$(CC) $(CFLAGS) -c src/Fornecedor.cpp

vector_supermercado.o: src/vector_supermercado.cpp include/vector_supermercado.h
	$(CC) $(CFLAGS) -c src/vector_supermercado.cpp

Excecao.o: src/Excecao.cpp include/Excecao.h
	$(CC) $(CFLAGS) -c src/Excecao.cpp

Util.o: src/Util.cpp include/Util.h
	$(CC) $(CFLAGS) -c src/Util.cpp