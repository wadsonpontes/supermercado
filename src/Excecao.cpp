#include "Excecao.h"

#include "Util.h"

#include <iostream>
#include <vector>
#include <sstream>

Excecao::Excecao() {

}

void Excecao::comando_inexistente() {
	limparTela();
	std::cout << "Comando inexistente!" << std::endl << std::endl;
	pausar();
}