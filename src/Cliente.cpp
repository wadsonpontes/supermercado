#include "Cliente.h"

#include "Util.h"
#include "Estabelecimento.h"

#include <iostream>
#include <vector>
#include <sstream>

Cliente::Cliente(int id) {
	this->id = id;
	this->saldo = 1045.00;
}

void Cliente::comprar(Produto produto, int quantidade) {
	this->saldo -= produto.preco * quantidade;

	if (this->sacola.existe(produto)) {
		this->sacola[produto].quantidade += quantidade;
	}
	else {
		produto.quantidade = quantidade;
		this->sacola.push(produto);
	}
}

void Cliente::comer(Produto produto, int quantidade) {
	this->saldo -= produto.preco * quantidade;

	if (this->comido.existe(produto)) {
		this->comido[produto].quantidade += quantidade;
	}
	else {
		produto.quantidade = quantidade;
		this->comido.push(produto);
	}
}

void Cliente::verSacola() {
	double total_compras = 0;

	for (size_t i = 0; i < this->sacola.tamanho; ++i) {
		std::cout << this->sacola[i].produto << " ";
		std::cout << this->sacola[i].unidadeMedida << " ";
		std::cout << "R$ " << this->sacola[i].preco << " ";
		std::cout << this->sacola[i].quantidade << std::endl;
		total_compras += this->sacola[i].preco * this->sacola[i].quantidade;
	}
	std::cout << std::endl << "Total de Compras = R$ ";
	std::cout << total_compras << std::endl << std::endl;
}

void Cliente::verConta() {
	double total_conta = 0;

	for (size_t i = 0; i < this->comido.tamanho; ++i) {
		std::cout << this->comido[i].produto << " ";
		std::cout << "R$ " << this->comido[i].preco << " ";
		std::cout << this->comido[i].quantidade << std::endl;
		total_conta += this->comido[i].preco * this->comido[i].quantidade;
	}
	std::cout << std::endl << "Total da Conta = R$ ";
	std::cout << total_conta << std::endl << std::endl;
}

void Cliente::registro_supermercado() {
	double total_compras = 0;
	std::stringstream stream;

	stream << "cliente_supermercado_";
	stream << this->id;
	stream << ".csv";

	std::string nome;
	stream >> nome;
	std::ofstream arquivo = open_file_for_writing(nome);

	arquivo << "COD,PRODUTO,UNIDADE DE MEDIDA,PREÇO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->sacola.tamanho; ++i) {
		arquivo << this->sacola[i].codigo << ",";
		arquivo << this->sacola[i].produto << ",";
		arquivo << this->sacola[i].unidadeMedida << ",";
		arquivo << "R$ " << this->sacola[i].preco << ",";
		arquivo << this->sacola[i].quantidade << std::endl;
		total_compras += this->sacola[i].preco * this->sacola[i].quantidade;
	}

	arquivo << std::endl << "Total de Vendas = R$ ";
	arquivo << total_compras << std::endl;

	arquivo.close();
}

void Cliente::registro_restaurante() {
	double total_comida = 0;
	std::stringstream stream;

	stream << "cliente_restaurante_";
	stream << this->id;
	stream << ".csv";

	std::string nome;
	stream >> nome;
	std::ofstream arquivo = open_file_for_writing(nome);

	arquivo << "PRODUTO,PREÇO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->comido.tamanho; ++i) {
		arquivo << this->comido[i].produto << ",";
		arquivo << "R$ " << this->comido[i].preco << ",";
		arquivo << this->comido[i].quantidade << std::endl;
		total_comida += this->comido[i].preco * this->comido[i].quantidade;
	}

	arquivo << std::endl << "Total em Refeições = R$ ";
	arquivo << total_comida << std::endl;

	arquivo.close();
}