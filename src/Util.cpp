#include "Util.h"

#include <iostream>

std::ifstream open_file_for_reading(std::string filename) {
	std::ifstream file(filename);
	check_file_for_error(!file);

	return file;
}

std::ofstream open_file_for_writing(std::string filename) {
	std::ofstream file(filename);
	
	check_file_for_error(!file);

	return file;
}

void check_file_for_error(bool error) {
	if (error) {
		std::cerr << "Arquivo não existente ou sem permissão de leitura.";
		std::cerr << std::endl;
		exit(1);
	}
}

int limparTela() {
	return system(LIMPAR_TELA);
}

void pausar() {
	std::cout << "Digite ENTER para continuar...";
	getchar();
}

void comando_inexistente() {
	limparTela();

	std::cout << "Comando inexistente!" << std::endl << std::endl;

	pausar();
}