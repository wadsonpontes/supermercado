#include "Supermercado.h"

#include "Excecao.h"
#include "Util.h"
#include "Estabelecimento.h"

#include <iostream>
#include <vector>
#include <sstream>

Supermercado::Supermercado() {
	std::string linha;
	char discard;
	this->arquivo = open_file_for_reading("estoque.csv");

	getline(arquivo, linha);

	while (getline(arquivo, linha)) {
		std::stringstream stream;
		std::string resto;
		Produto produto;

		stream << linha;

		stream >> produto.codigo;
		stream >> discard;
		getline(stream, resto);

		std::size_t pos = resto.find(",");
		produto.produto = resto.substr(0, pos);
		resto = resto.substr(pos + 1);

		pos = resto.find(",");
		produto.unidadeMedida = resto.substr(0, pos);
		resto = resto.substr(pos + 4);

		std::stringstream stream2(resto);

		stream2 >> produto.preco;
		stream2 >> discard;
		stream2 >> produto.quantidade;

		this->produtos.push(produto);
	}

	this->arquivo.close();
}

Supermercado::~Supermercado() {
	double total_vendas = 0;
	std::ofstream arquivo = open_file_for_writing("caixa_supermercado.csv");

	arquivo << "COD,PRODUTO,UNIDADE DE MEDIDA,PREÇO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->vendidos.tamanho; ++i) {
		arquivo << this->vendidos[i].codigo << ",";
		arquivo << this->vendidos[i].produto << ",";
		arquivo << this->vendidos[i].unidadeMedida << ",";
		arquivo << "R$ " << this->vendidos[i].preco << ",";
		arquivo << this->vendidos[i].quantidade << std::endl;
		total_vendas += this->vendidos[i].preco * this->vendidos[i].quantidade;
	}

	arquivo << std::endl << "Total de Vendas = R$ ";
	arquivo << total_vendas << std::endl;

	arquivo.close();
}

void Supermercado::listar() {
	for (size_t i = 0; i < this->produtos.tamanho; ++i) {
		std::cout << this->produtos[i].codigo << ") ";
		std::cout << this->produtos[i].produto << " ";
		std::cout << this->produtos[i].unidadeMedida << " ";
		std::cout << "R$ " << this->produtos[i].preco << " ";
		std::cout << this->produtos[i].quantidade << std::endl;
	}
	std::cout << std::endl;
}

void Supermercado::vender(std::string produto, int quantidade, Cliente &cliente) {
	this->produtos[produto].quantidade -= quantidade;

	if (this->vendidos.existe(produto)) {
		this->vendidos[produto].quantidade += quantidade;
	}
	else {
		this->vendidos.push(this->produtos[produto]);
		this->vendidos[produto].quantidade = quantidade;
	}

	cliente.comprar(this->produtos[produto], quantidade);
}

void Supermercado::reabastecer(std::string p, int q, Fornecedor &f) {
	this->produtos[p].quantidade += f.repassarProdutos(p, q);
}

void Supermercado::caixa() {
	double total_vendas = 0;
	
	for (size_t i = 0; i < this->vendidos.tamanho; ++i) {
		std::cout << this->vendidos[i].codigo << " ";
		std::cout << this->vendidos[i].produto << " ";
		std::cout << this->vendidos[i].unidadeMedida << " ";
		std::cout << "R$ " << this->vendidos[i].preco << " ";
		std::cout << this->vendidos[i].quantidade << std::endl;
		total_vendas += this->vendidos[i].preco * this->vendidos[i].quantidade;
	}

	std::cout << std::endl << "Total de Vendas = R$ ";
	std::cout << total_vendas << std::endl << std::endl;
}

void Supermercado::imprimir_menu(Cliente &cliente) {
	limparTela();

	std::cout << "Seu saldo é: R$ " << cliente.saldo << std::endl << std::endl;

	std::cout << "Selecione uma ação:" << std::endl << std::endl;

	std::cout << "1) Listar produtos em estoque" << std::endl;
	std::cout << "2) Caixa" << std::endl;
	std::cout << "3) Comprar" << std::endl;
	std::cout << "4) Ver sua sacola" << std::endl;
	std::cout << "5) Listar produtos do fornecedor" << std::endl;
	std::cout << "6) Reabastecer estoque" << std::endl;

	std::cout << "0) Sair do supermercado" << std::endl;

	std::cout << ">";
}

void Supermercado::listar_produtos_estoque() {
	std::cout << "Lista de produtos em estoque:" << std::endl << std::endl;

	this->listar();

	pausar();
}

void Supermercado::listar_caixa() {
	std::cout << "Lista de produtos vendidos:" << std::endl << std::endl;

	this->caixa();

	pausar();
}

void Supermercado::iniciar_compra(Cliente &cliente) {
	std::string produto;
	int quantidade;

	std::cout << "Escolha o produto que deseja comprar:" << std::endl << std::endl;

	this->listar();

	std::cout << ">";
	getline(std::cin, produto);
	limparTela();

	if (this->produtos.existe(produto)) {
		std::cout << "Escolha a quantidade que vai levar:" << std::endl << std::endl;

		std::cout << ">";
		std::cin >> quantidade;
		getchar();
		limparTela();

		if (this->produtos[produto].quantidade >= quantidade) {
			if (this->produtos[produto].preco * quantidade <= cliente.saldo) {
				this->vender(produto, quantidade, cliente);

				std::cout << "Compra realizada com sucesso!" << std::endl << std::endl;
			}
			else {
				std::cout << "Saldo insuficiente!" << std::endl << std::endl;
			}
		}
		else {
			std::cout << "Não temos essa quantidade de produto!" << std::endl << std::endl;
		}
	}
	else {
		std::cout << "Produto inexistente!" << std::endl << std::endl;
	}

	pausar();
}

void Supermercado::ver_sacola(Cliente &cliente) {
	std::cout << "Produtos na sua sacola:" << std::endl << std::endl;

	cliente.verSacola();

	pausar();
}

void Supermercado::listar_produtos_fornecedor(Fornecedor &fornecedor) {
	std::cout << "Lista de produtos do fornecedor:" << std::endl << std::endl;

	fornecedor.listarProdutos();

	pausar();
}

void Supermercado::iniciar_reabastecimento(Fornecedor &fornecedor) {
	std::string produto;
	int quantidade;

	std::cout << "Escolha o produto:" << std::endl << std::endl;

	fornecedor.listarProdutos();

	std::cout << ">";
	getline(std::cin, produto);
	limparTela();

	if (fornecedor.produtos.existe(produto)) {
		std::cout << "Escolha a quantidade:" << std::endl << std::endl;

		std::cout << ">";
		std::cin >> quantidade;
		getchar();
		limparTela();

		if (fornecedor.produtos[produto].quantidade >= quantidade) {
			this->reabastecer(produto, quantidade, fornecedor);

			std::cout << "Fornecido com sucesso!" << std::endl << std::endl;
		}
		else {
			std::cout << "Quantidade inexistente!" << std::endl << std::endl;
		}
	}
	else {
		std::cout << "Produto inexistente!" << std::endl << std::endl;
	}

	pausar();
}

void Supermercado::sair() {
	std::cout << "Volte sempre!" << std::endl << std::endl;

	pausar();
}

void Supermercado::entrar(Fornecedor &fornecedor, Cliente &cliente) {
	std::string acao;

	do {
		try {
			this->imprimir_menu(cliente);
			getline(std::cin, acao);
			limparTela();

			if (acao == "1") {
				this->listar_produtos_estoque();
			}
			else if (acao == "2") {
				this->listar_caixa();
			}
			else if (acao == "3") {
				this->iniciar_compra(cliente);
			}
			else if (acao == "4") {
				this->ver_sacola(cliente);
			}
			else if (acao == "5") {
				this->listar_produtos_fornecedor(fornecedor);
			}
			else if (acao == "6") {
				this->iniciar_reabastecimento(fornecedor);
			}
			else if (acao == "0") {
				this->sair();
			}
			else {
				throw (Excecao());
			}
		}
		catch (Excecao &e) {
			e.comando_inexistente();
		}

	} while (acao != "0");
}