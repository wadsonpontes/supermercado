#include "Restaurante.h"
#include "Supermercado.h"
#include "Estabelecimento.h"
#include "Fornecedor.h"
#include "Cliente.h"
#include "Excecao.h"
#include "Util.h"

#include <iostream>

void imprimir_menu();
void adicionar_saldo(Cliente &cliente);
void sair(Cliente &cliente, std::string &acao, int &id);

int main(int argc, char* argv[]) {
	std::string acao;
	int id = 1;
	Supermercado supermercado;
	Fornecedor fornecedor;
	Restaurante restaurante;
	Cliente cliente(id);

	do {
		try {
			imprimir_menu();
			getline(std::cin, acao);
			limparTela();

			if (acao == "1") {
				supermercado.entrar(fornecedor, cliente);
			}
			else if (acao == "2") {
				restaurante.entrar(cliente);
			}
			else if (acao == "3") {
				adicionar_saldo(cliente);
			}
			else if (acao == "0") {
				sair(cliente, acao, id);
			}
			else {
				throw (Excecao());
			}
		}
		catch (Excecao &e) {
			e.comando_inexistente();
		}		
	} while (acao != "0");

	return 0;
}

void imprimir_menu() {
	limparTela();

	std::cout << "BEM-VINDO AO SHOPPING LPI" << std::endl << std::endl;

	std::cout << "Selecione uma ação:" << std::endl << std::endl;

	std::cout << "1) Entrar no supermercado" << std::endl;
	std::cout << "2) Entrar no restaurante" << std::endl;
	std::cout << "3) Entrar no banco (sacar)" << std::endl;
	
	std::cout << "0) Sair do Shopping" << std::endl;

	std::cout << ">";
}

void adicionar_saldo(Cliente &cliente) {
	double saldo;

	std::cout << "Quando você pretende sacar?" << std::endl << std::endl;

	std::cout << ">";
	std::cin >> saldo;
	getchar();

	cliente.saldo += saldo;

	limparTela();
	std::cout << "Saldo adicionado com sucesso!" << std::endl << std::endl;

	pausar();
}

void sair(Cliente &cliente, std::string &acao, int &id) {
	std::cout << "Selecione uma ação:" << std::endl << std::endl;

	std::cout << "1) Simular outro cliente" << std::endl;
	std::cout << "0) Finalizar programa" << std::endl;

	std::cout << ">";
	getline(std::cin, acao);

	cliente.registro_supermercado();
	cliente.registro_restaurante();

	id++;

	Cliente c(id);

	cliente = c;
}