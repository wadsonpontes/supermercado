#include "Fornecedor.h"

#include "Produto.h"
#include "Util.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <string>

Fornecedor::Fornecedor() {
	std::string linha;

	this->arquivo_leitura = open_file_for_reading("fornecedor.csv");

	getline(arquivo_leitura, linha);

	while (getline(arquivo_leitura, linha)) {
		Produto produto;

		std::size_t pos = linha.find(",");
		produto.produto = linha.substr(0, pos);
		produto.quantidade = stoi(linha.substr(pos + 1));

		this->produtos.push(produto);
	}

	this->arquivo_leitura.close();
}

Fornecedor::~Fornecedor() {
	this->salvar();
}

void Fornecedor::listarProdutos() {
	std::cout << "PRODUTO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->produtos.tamanho; ++i) {
		if (this->produtos[i].quantidade > 0) {
			std::cout << this->produtos[i].produto;
			std::cout << ",";
			std::cout << this->produtos[i].quantidade << std::endl;
		}
	}
	std::cout << std::endl;
}

int Fornecedor::repassarProdutos(std::string produto, int quantidade) {
	if (this->produtos[produto].quantidade < quantidade)
		return 0;

	this->produtos[produto].quantidade -= quantidade;

	return quantidade;
}

void Fornecedor::salvar() {
	this->arquivo_escrita = open_file_for_writing("fornecedor.csv");

	arquivo_escrita << "PRODUTO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->produtos.tamanho; ++i) {
		arquivo_escrita << this->produtos[i].produto;
		arquivo_escrita << ",";
		arquivo_escrita << this->produtos[i].quantidade << std::endl;
	}
}