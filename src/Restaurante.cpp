#include "Restaurante.h"

#include "Util.h"
#include "Excecao.h"
#include "Estabelecimento.h"

#include <iostream>
#include <vector>
#include <sstream>

Restaurante::Restaurante() {
	std::string linha;
	this->arquivo = open_file_for_reading("menu.csv");

	getline(arquivo, linha);

	while (getline(arquivo, linha)) {
		std::stringstream stream;
		Produto produto;

		std::size_t pos = linha.find(",");
		produto.produto = linha.substr(0, pos);
		linha = linha.substr(pos + 4);

		stream << linha;
		stream >> produto.preco;

		produto.quantidade = -1;

		this->produtos.push(produto);
	}

	this->arquivo.close();
}

Restaurante::~Restaurante() {
	double total_refeicoes = 0;
	std::ofstream arquivo = open_file_for_writing("caixa_restaurante.csv");

	arquivo << "PRODUTO,PREÇO,QUANTIDADE" << std::endl;

	for (size_t i = 0; i < this->vendidos.tamanho; ++i) {
		arquivo << this->vendidos[i].produto << ",";
		arquivo << "R$ " << this->vendidos[i].preco << ",";
		arquivo << this->vendidos[i].quantidade << std::endl;
		total_refeicoes += this->vendidos[i].preco * this->vendidos[i].quantidade;
	}

	arquivo << std::endl << "Total em Refeições = R$ ";
	arquivo << total_refeicoes << std::endl;

	arquivo.close();
}

void Restaurante::listar() {
	for (size_t i = 0; i < this->produtos.tamanho; ++i) {
		std::cout << this->produtos[i].produto << " ";
		std::cout << "R$ " << this->produtos[i].preco << std::endl;
	}
	std::cout << std::endl;
}

void Restaurante::vender(std::string produto, int quantidade, Cliente &cliente) {
	this->produtos[produto].quantidade -= quantidade;

	if (this->vendidos.existe(produto)) {
		this->vendidos[produto].quantidade += quantidade;
	}
	else {
		this->vendidos.push(this->produtos[produto]);
		this->vendidos[produto].quantidade = quantidade;
	}

	cliente.comer(this->produtos[produto], quantidade);
}

void Restaurante::caixa() {
	double total_refeicoes = 0;
	
	for (size_t i = 0; i < this->vendidos.tamanho; ++i) {
		std::cout << this->vendidos[i].produto << " ";
		std::cout << "R$ " << this->vendidos[i].preco << " ";
		std::cout << this->vendidos[i].quantidade << std::endl;
		total_refeicoes += this->vendidos[i].preco * this->vendidos[i].quantidade;
	}

	std::cout << std::endl << "Total em Refeições = R$ ";
	std::cout << total_refeicoes << std::endl << std::endl;
}

void Restaurante::imprimir_menu(Cliente &cliente) {
	limparTela();

	std::cout << "Seu saldo é: R$ " << cliente.saldo << std::endl << std::endl;

	std::cout << "Selecione uma ação:" << std::endl << std::endl;

	std::cout << "1) Ver cardápio" << std::endl;
	std::cout << "2) Caixa" << std::endl;
	std::cout << "3) Iniciar pedido" << std::endl;
	std::cout << "4) Ver conta" << std::endl;

	std::cout << "0) Sair do restaurante" << std::endl;

	std::cout << ">";
}

void Restaurante::listar_produtos_menu() {
	std::cout << "Cardápio:" << std::endl << std::endl;

	this->listar();

	pausar();
}

void Restaurante::listar_caixa() {
	std::cout << "Lista de pratos vendidos:" << std::endl << std::endl;

	this->caixa();

	pausar();
}

void Restaurante::iniciar_pedido(Cliente &cliente) {
	std::string produto;
	int quantidade;

	std::cout << "O que deseja comer agora?" << std::endl << std::endl;

	this->listar();

	std::cout << ">";
	getline(std::cin, produto);
	limparTela();

	if (this->produtos.existe(produto)) {
		std::cout << "Quantos você quer?" << std::endl << std::endl;

		std::cout << ">";
		std::cin >> quantidade;
		getchar();
		limparTela();

		if (this->produtos[produto].preco * quantidade <= cliente.saldo) {
			this->vender(produto, quantidade, cliente);

			std::cout << "Seu pedido foi entregue!" << std::endl << std::endl;
		}
		else {
			std::cout << "Você não tem dinheiro suficiente!" << std::endl << std::endl;
		}
	}
	else {
		std::cout << "Não temos este prato! Você leu o cardápio?" << std::endl << std::endl;
	}

	pausar();
}

void Restaurante::ver_conta(Cliente &cliente) {
	std::cout << "Sua conta:" << std::endl << std::endl;

	cliente.verConta();

	pausar();
}

void Restaurante::sair() {
	std::cout << "Volte sempre!" << std::endl << std::endl;

	pausar();
}

void Restaurante::entrar(Cliente &cliente) {
	std::string acao;

	do {
		try {
			this->imprimir_menu(cliente);
			getline(std::cin, acao);
			limparTela();

			if (acao == "1") {
				this->listar_produtos_menu();
			}
			else if (acao == "2") {
				this->listar_caixa();
			}
			else if (acao == "3") {
				this->iniciar_pedido(cliente);
			}
			else if (acao == "4") {
				this->ver_conta(cliente);
			}
			else if (acao == "0") {
				this->sair();
			}
			else {
				throw (Excecao());
			}
		}
		catch (Excecao &e) {
			e.comando_inexistente();
		}

	} while (acao != "0");
}